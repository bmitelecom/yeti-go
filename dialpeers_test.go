package yetiapi

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)


func TestDialpeersAPI_GetDialpeerByID(t *testing.T) {
	ctx := context.Background()

	sample := `{"data":{"id":"2722424","type":"dialpeers","links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424"},"attributes":{"enabled":true,"next-rate":"0.0","connect-fee":"0.0","initial-rate":"0.0","initial-interval":1,"next-interval":1,"valid-from":"2017-07-03T04:25:00.000Z","valid-till":"2025-06-06T12:20:00.000Z","prefix":"78005511410","src-rewrite-rule":"","dst-rewrite-rule":"","acd-limit":0.0,"asr-limit":0.0,"src-rewrite-result":"","dst-rewrite-result":"","locked":false,"priority":100,"exclusive-route":true,"capacity":2,"lcr-rate-multiplier":"1.0","force-hit-rate":null,"network-prefix-id":34729,"created-at":"2020-02-13T11:46:14.137Z","short-calls-limit":1.0,"external-id":null,"routing-tag-ids":[null],"dst-number-min-length":0,"dst-number-max-length":100},"relationships":{"gateway":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway"}},"gateway-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway-group"}},"routing-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-group"}},"vendor":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/vendor","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/vendor"}},"account":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/account","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/account"}},"routing-tag-mode":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-tag-mode","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-tag-mode"}},"routeset-discriminator":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routeset-discriminator","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routeset-discriminator"}},"dialpeer-next-rates":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/dialpeer-next-rates","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/dialpeer-next-rates"}}}}}`

	mock := RunMockServer(t, sample)

	cfg := NewConfiguration()
	cfg.BasicAuth = &BasicAuth{UserName: "admin", Password: "admin"}

	clt, err := NewAPIClient(cfg)
	if err != nil {
		assert.NoError(t, err)
	}
	clt.cfg.Host = mock.URL
	clt.cfg.HTTPClient = mock.Client()

	dp, err := clt.DialpeersAPI.GetDialpeerByID(ctx, "2722424")
	if err != nil {
		assert.NoError(t, err)
	}

	assert.NotNil(t, dp, "Must not be nil")
	assert.Equal(t, "2722424", dp.ID)
	assert.Equal(t, true, dp.Attributes.Enabled)
	assert.Equal(t, "http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway", dp.Relationships.Gateway.Links.Self)

}

func TestDialpeersAPI_CreateDialpeer(t *testing.T) {
	ctx := context.Background()

	sample := `{"data":{"id":"2722424","type":"dialpeers","links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424"},"attributes":{"enabled":true,"next-rate":"0.0","connect-fee":"0.0","initial-rate":"0.0","initial-interval":1,"next-interval":1,"valid-from":"2017-07-03T04:25:00.000Z","valid-till":"2025-06-06T12:20:00.000Z","prefix":"78005511410","src-rewrite-rule":"","dst-rewrite-rule":"","acd-limit":0.0,"asr-limit":0.0,"src-rewrite-result":"","dst-rewrite-result":"","locked":false,"priority":100,"exclusive-route":true,"capacity":2,"lcr-rate-multiplier":"1.0","force-hit-rate":null,"network-prefix-id":34729,"created-at":"2020-02-13T11:46:14.137Z","short-calls-limit":1.0,"external-id":null,"routing-tag-ids":[null],"dst-number-min-length":0,"dst-number-max-length":100},"relationships":{"gateway":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway"}},"gateway-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway-group"}},"routing-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-group"}},"vendor":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/vendor","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/vendor"}},"account":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/account","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/account"}},"routing-tag-mode":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-tag-mode","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-tag-mode"}},"routeset-discriminator":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routeset-discriminator","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routeset-discriminator"}},"dialpeer-next-rates":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/dialpeer-next-rates","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/dialpeer-next-rates"}}}}}`

	mock := RunMockServer(t, sample)

	cfg := NewConfiguration()
	cfg.BasicAuth = &BasicAuth{UserName: "admin", Password: "admin"}

	clt, err := NewAPIClient(cfg)
	assert.NoError(t, err)

	clt.cfg.Host = mock.URL
	clt.cfg.HTTPClient = mock.Client()

	dp := &DialpeerInput{
		Type: "dialpeer",
		Attributes: DialpeerAttributes {
			Enabled: true,
			NextRate: 0.0,
			ConnectFee: 0.0,
			InitialRate: 0.0,
			InitialInterval: 1,
			NextInterval: 1,
			ValidFrom: time.Now(),
			ValidTill: time.Date(2030, 12, 31, 23, 59, 59, 0, time.UTC),
			Capacity: 2,
			ShortCallsLimit: 1.0,
		},
	}
	dp.SetRoutingGroup("10")
	dp.SetAccount("1")
	dp.SetVendor("2")
	dp.SetRoutesetDiscriminator("2")

	_, err = clt.DialpeersAPI.CreateDialpeer(ctx, dp)
	assert.NoError(t, err)
}

func TestDialpeersAPI_ListDialpeers(t *testing.T) {
	ctx := context.Background()

	sample := `{
    "data": [
        {
            "id": "1594734",
            "type": "dialpeers",
            "links": {
                "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734"
            },
            "attributes": {
                "enabled": true,
                "next-rate": "0.0",
                "connect-fee": "0.0",
                "initial-rate": "0.0",
                "initial-interval": 1,
                "next-interval": 1,
                "valid-from": "2016-12-16T08:35:00.000Z",
                "valid-till": "2020-06-06T12:20:00.000Z",
                "prefix": "74955851400",
                "src-rewrite-rule": "",
                "dst-rewrite-rule": "",
                "acd-limit": 0.0,
                "asr-limit": 0.0,
                "src-rewrite-result": "",
                "dst-rewrite-result": "",
                "locked": false,
                "priority": 100,
                "exclusive-route": true,
                "capacity": 10,
                "lcr-rate-multiplier": "1.0",
                "force-hit-rate": null,
                "network-prefix-id": 4707,
                "created-at": "2016-06-08T04:25:15.651Z",
                "short-calls-limit": 1.0,
                "external-id": null,
                "routing-tag-ids": [],
                "dst-number-min-length": 0,
                "dst-number-max-length": 100
            },
            "relationships": {
                "gateway": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/gateway",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/gateway"
                    }
                },
                "gateway-group": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/gateway-group",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/gateway-group"
                    }
                },
                "routing-group": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/routing-group",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/routing-group"
                    }
                },
                "vendor": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/vendor",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/vendor"
                    }
                },
                "account": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/account",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/account"
                    }
                },
                "routing-tag-mode": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/routing-tag-mode",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/routing-tag-mode"
                    }
                },
                "routeset-discriminator": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/routeset-discriminator",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/routeset-discriminator"
                    }
                },
                "dialpeer-next-rates": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/dialpeer-next-rates",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/1594734/dialpeer-next-rates"
                    }
                }
            }
        },
        {
            "id": "2556445",
            "type": "dialpeers",
            "links": {
                "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445"
            },
            "attributes": {
                "enabled": true,
                "next-rate": "0.0",
                "connect-fee": "0.0",
                "initial-rate": "0.0",
                "initial-interval": 1,
                "next-interval": 1,
                "valid-from": "2016-12-16T08:35:00.000Z",
                "valid-till": "2020-06-06T12:20:00.000Z",
                "prefix": "74955851400",
                "src-rewrite-rule": "",
                "dst-rewrite-rule": "",
                "acd-limit": 0.0,
                "asr-limit": 0.0,
                "src-rewrite-result": "",
                "dst-rewrite-result": "",
                "locked": false,
                "priority": 100,
                "exclusive-route": true,
                "capacity": 10,
                "lcr-rate-multiplier": "1.0",
                "force-hit-rate": null,
                "network-prefix-id": 4707,
                "created-at": "2019-07-09T08:35:19.979Z",
                "short-calls-limit": 1.0,
                "external-id": null,
                "routing-tag-ids": [
                    null
                ],
                "dst-number-min-length": 0,
                "dst-number-max-length": 100
            },
            "relationships": {
                "gateway": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/gateway",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/gateway"
                    }
                },
                "gateway-group": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/gateway-group",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/gateway-group"
                    }
                },
                "routing-group": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/routing-group",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/routing-group"
                    }
                },
                "vendor": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/vendor",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/vendor"
                    }
                },
                "account": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/account",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/account"
                    }
                },
                "routing-tag-mode": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/routing-tag-mode",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/routing-tag-mode"
                    }
                },
                "routeset-discriminator": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/routeset-discriminator",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/routeset-discriminator"
                    }
                },
                "dialpeer-next-rates": {
                    "links": {
                        "self": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/relationships/dialpeer-next-rates",
                        "related": "http://10.2.0.30/api/rest/admin/dialpeers/2556445/dialpeer-next-rates"
                    }
                }
            }
        }
    ],
    "meta": {
        "total-count": 2
    }
}`

	mock := RunMockServer(t, sample)

	cfg := NewConfiguration()
	cfg.BasicAuth = &BasicAuth{UserName: "admin", Password: "admin"}

	clt, err := NewAPIClient(cfg)
	if err != nil {
		assert.NoError(t, err)
	}
	clt.cfg.Host = mock.URL
	clt.cfg.HTTPClient = mock.Client()

	list, err := clt.DialpeersAPI.ListDialpeers(ctx, nil)
	assert.NoError(t, err)

	assert.NotNil(t, list[0], "Must not be nil")
	assert.Equal(t, "1594734", list[0].ID)
	assert.Equal(t, true, list[0].Attributes.Enabled)
	assert.Equal(t, "http://10.2.0.30/api/rest/admin/dialpeers/1594734/relationships/gateway", list[0].Relationships.Gateway.Links.Self)

	filter := &DialpeerFilters{}
	filter.Prefix = "74955851400"
	_, err = clt.DialpeersAPI.ListDialpeers(ctx, filter)
	assert.NoError(t, err)


}

func TestDialpeersAPI_DialpeerOutputParse(t *testing.T) {
	output := `{"data":{"id":"2722424","type":"dialpeers","links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424"},"attributes":{"enabled":true,"next-rate":"0.0","connect-fee":"0.0","initial-rate":"0.0","initial-interval":1,"next-interval":1,"valid-from":"2017-07-03T04:25:00.000Z","valid-till":"2025-06-06T12:20:00.000Z","prefix":"78001111111","src-rewrite-rule":"","dst-rewrite-rule":"","acd-limit":0.0,"asr-limit":0.0,"src-rewrite-result":"","dst-rewrite-result":"","locked":false,"priority":100,"exclusive-route":true,"capacity":2,"lcr-rate-multiplier":"1.0","force-hit-rate":null,"network-prefix-id":34729,"created-at":"2020-02-13T11:46:14.137Z","short-calls-limit":1.0,"external-id":null,"routing-tag-ids":[null],"dst-number-min-length":0,"dst-number-max-length":100},"relationships":{"gateway":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway"}},"gateway-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/gateway-group"}},"routing-group":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-group","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-group"}},"vendor":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/vendor","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/vendor"}},"account":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/account","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/account"}},"routing-tag-mode":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routing-tag-mode","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routing-tag-mode"}},"routeset-discriminator":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/routeset-discriminator","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/routeset-discriminator"}},"dialpeer-next-rates":{"links":{"self":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/dialpeer-next-rates","related":"http://10.2.0.30/api/rest/admin/dialpeers/2722424/dialpeer-next-rates"}}}}}`

	dp := &DialpeerOutput{}
	env := &ResponseEnvelope{Data: dp}

	err := json.Unmarshal([]byte(output), env)
	assert.NoError(t, err)

	assert.Equal(t,  "2722424", dp.ID)
	assert.Equal(t, true, dp.Attributes.Enabled)
	assert.Equal(t, "http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway", dp.Relationships.Gateway.Links.Self)
}

func TestDialpeersAPI_DialpeerInputParse(t *testing.T) {
	dp := &DialpeerInput{
		Type: "dialpeer",
		Attributes: DialpeerAttributes {
			Enabled: true,
			NextRate: 0.0,
			ConnectFee: 0.0,
			InitialRate: 0.0,
			InitialInterval: 1,
			NextInterval: 1,
			ValidFrom: time.Now(),
			ValidTill: time.Date(2030, 12, 31, 23, 59, 59, 0, time.UTC),
			Capacity: 2,
			ShortCallsLimit: 1.0,
		},
	}
	dp.SetRoutingGroup("10")
	dp.SetAccount("1")
	dp.SetVendor("2")
	dp.SetRoutesetDiscriminator("2")

	env := &ResponseEnvelope{Data: dp}

	input, err := json.Marshal(env)
	assert.NoError(t, err)
	t.Log(string(input))
}

func TestDialpeersAPI_ListOfDialpeersParse(t *testing.T) {
	output := `{
  "data":[
    {
      "id":"1594734",
      "type":"dialpeers",
      "links":{
        "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734"
      },
      "attributes":{
        "enabled":true,
        "next-rate":"0.0",
        "connect-fee":"0.0",
        "initial-rate":"0.0",
        "initial-interval":1,
        "next-interval":1,
        "valid-from":"2016-12-16T08:35:00.000Z",
        "valid-till":"2020-06-06T12:20:00.000Z",
        "prefix":"74955851400",
        "src-rewrite-rule":"",
        "dst-rewrite-rule":"",
        "acd-limit":0,
        "asr-limit":0,
        "src-rewrite-result":"",
        "dst-rewrite-result":"",
        "locked":false,
        "priority":100,
        "exclusive-route":true,
        "capacity":10,
        "lcr-rate-multiplier":"1.0",
        "force-hit-rate":null,
        "network-prefix-id":4707,
        "created-at":"2016-06-08T04:25:15.651Z",
        "short-calls-limit":1,
        "external-id":null,
        "routing-tag-ids":[
          
        ],
        "dst-number-min-length":0,
        "dst-number-max-length":100
      },
      "relationships":{
        "gateway":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/gateway",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/gateway"
          }
        },
        "gateway-group":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/gateway-group",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/gateway-group"
          }
        },
        "routing-group":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/routing-group",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/routing-group"
          }
        },
        "vendor":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/vendor",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/vendor"
          }
        },
        "account":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/account",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/account"
          }
        },
        "routing-tag-mode":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/routing-tag-mode",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/routing-tag-mode"
          }
        },
        "routeset-discriminator":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/routeset-discriminator",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/routeset-discriminator"
          }
        },
        "dialpeer-next-rates":{
          "links":{
            "self":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/relationships\/dialpeer-next-rates",
            "related":"http:\/\/10.2.0.30\/api\/rest\/admin\/dialpeers\/1594734\/dialpeer-next-rates"
          }
        }
      }
    }
  ],
  "meta":{
    "total-count":2
  }
}`
	dpList := &[]*DialpeerOutput{}
	env := &ResponseEnvelope{Data: dpList}

	err := json.Unmarshal([]byte(output), env)
	assert.NoError(t, err)

	dpList1 := *dpList

	t.Log(dpList)

	//assert.NotNil(t, dpList[0])
	assert.Equal(t,  2, env.Meta.TotalCount)
	assert.Equal(t, true, dpList1[0].Attributes.Enabled)
	//assert.Equal(t, "http://10.2.0.30/api/rest/admin/dialpeers/2722424/relationships/gateway", dpList[0].Relationships.Gateway.Links.Self)
}