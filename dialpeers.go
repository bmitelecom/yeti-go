package yetiapi

import (
	"context"
	"io/ioutil"
	"net/url"
	"time"
)

type DialpeersService service

//Dialpeer represents Dialpeer entity in Yeti database
type DialpeerInput struct {
	Type          string                     `json:"type"` //required
	Attributes    DialpeerAttributes         `json:"attributes"`
	Relationships DialpeerRelationshipsInput `json:"relationships"`
}

type DialpeerOutput struct {
	ID            string                      `json:"id"`
	Type          string                      `json:"type"` //required
	Links         Links                       `json:"links"`
	Attributes    DialpeerAttributes          `json:"attributes"`
	Relationships DialpeerRelationshipsOutput `json:"relationships"`
}

type DialpeerAttributes struct {
	Enabled            bool          `json:"enabled"`             //required
	NextRate           float64       `json:"next-rate,string"`    //required
	ConnectFee         float64       `json:"connect-fee,string"`  //required
	InitialRate        float64       `json:"initial-rate,string"` //required
	InitialInterval    int           `json:"initial-interval"`    //required
	NextInterval       int           `json:"next-interval"`       //required
	ValidFrom          time.Time     `json:"valid-from"`          //required
	ValidTill          time.Time     `json:"valid-till"`          //required
	Prefix             string        `json:"prefix"`
	SrcRewriteRule     interface{}   `json:"src-rewrite-rule"`
	DstRewriteRule     interface{}   `json:"dst-rewrite-rule"`
	AcdLimit           float64       `json:"acd-limit"`
	AsrLimit           float64       `json:"asr-limit"`
	SrcRewriteResult   interface{}   `json:"src-rewrite-result"`
	DstRewriteResult   interface{}   `json:"dst-rewrite-result"`
	Locked             bool          `json:"locked"`
	Priority           int           `json:"priority"`
	ExclusiveRoute     bool          `json:"exclusive-route"`
	Capacity           int           `json:"capacity"`
	LcrRateMultiplier  string        `json:"lcr-rate-multiplier"`
	ForceHitRate       interface{}   `json:"force-hit-rate"`
	NetworkPrefixID    interface{}   `json:"network-prefix-id"`
	CreatedAt          time.Time     `json:"created-at"`
	ShortCallsLimit    float64       `json:"short-calls-limit"`
	ExternalID         int           `json:"external-id"`
	RoutingTagIds      []interface{} `json:"routing-tag-ids"`
	DstNumberMinLength int           `json:"dst-number-min-length"`
	DstNumberMaxLength int           `json:"dst-number-max-length"`
}

type DialpeerLink struct {
	Links Links `json:"links"`
}

type DialpeerRelationshipsInput struct {
	Gateway               DialpeerRelationEnvelope `json:"gateway"`
	GatewayGroup          DialpeerRelationEnvelope `json:"gateway-group"`
	RoutingGroup          DialpeerRelationEnvelope `json:"routing-group"`
	Vendor                DialpeerRelationEnvelope `json:"vendor"`
	Account               DialpeerRelationEnvelope `json:"account"`
	RoutingTagMode        DialpeerRelationEnvelope `json:"routing-tag-mode"`
	RoutesetDiscriminator DialpeerRelationEnvelope `json:"routeset-discriminator"`
	DialpeerNextRates     DialpeerRelationEnvelope `json:"dialpeer-next-rates"`
}

type DialpeerRelationshipsOutput struct {
	Gateway               Links `json:"gateway"`
	GatewayGroup          Links `json:"gateway-group"`
	RoutingGroup          Links `json:"routing-group"`
	Vendor                Links `json:"vendor"`
	Account               Links `json:"account"`
	RoutingTagMode        Links `json:"routing-tag-mode"`
	RoutesetDiscriminator Links `json:"routeset-discriminator"`
	DialpeerNextRates     Links `json:"dialpeer-next-rates"`
}

type DialpeerRelationEnvelope struct {
	Data DialpeerRelationEntity `json:"data"`
}

type DialpeerRelationEntity struct {
	ID   string `json:"id"`
	Type string `json:"type"`
}

type Links struct {
	Links Link `json:"links"`
}

type Link struct {
	Self    string `json:"self"`
	Related string `json:"related"`
}

type DialpeerFilters struct {
	Prefix string
	RoutingGroupID string
	ExternalID string
}

//SetRoutingGroup sets up RoutingGroup by given ID for a new Dialpeer
func (dp *DialpeerInput) SetRoutingGroup(id string) {
	dp.Relationships.RoutingGroup = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "routing-groups",
		},
	}
}

//SetVendor sets up a Vendor by given ID for a new Dialpeer
func (dp *DialpeerInput) SetVendor(id string) {
	dp.Relationships.Vendor = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "vendors",
		},
	}
}

//SetAccount sets up an Account by given ID for a new Dialpeer
func (dp *DialpeerInput) SetAccount(id string) {
	dp.Relationships.Account = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "accounts",
		},
	}
}

//SetRoutesetDiscriminator sets up a RouteSet Discriminator by given ID for a new Dialpeer
func (dp *DialpeerInput) SetRoutesetDiscriminator(id string) {
	dp.Relationships.RoutesetDiscriminator = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "routeset-discriminators",
		},
	}
}

//SetGateway sets up a Gateway by given ID for a new Dialpeer
func (dp *DialpeerInput) SetGateway(id string) {
	dp.Relationships.Gateway = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "gateways",
		},
	}
}

//SetGatewayGroup sets up a GatewayGroup by given ID for a new Dialpeer
func (dp *DialpeerInput) SetGatewayGroup(id string) {
	dp.Relationships.GatewayGroup = DialpeerRelationEnvelope{
		Data: DialpeerRelationEntity{
			ID:   id,
			Type: "gateway-groups",
		},
	}
}

func (api *DialpeersService) ListDialpeers(ctx context.Context, filters *DialpeerFilters) (list []*DialpeerOutput, err error) {
	var (
		localVarHTTPMethod = "GET"
		//localVarPostBody   interface{}
	)

	// create path and map variables
	localVarPath := api.client.cfg.Host + api.client.cfg.BasePath + "/dialpeers"

	filterQuery := url.Values{}
	formParams := url.Values{}

	//If there are filters, then we try to apply them
	if filters != nil {
		if filters.Prefix != "" {
			filterQuery.Add("filter[prefix]", filters.Prefix)
		}
		if filters.RoutingGroupID != "" {
			filterQuery.Add("routing_group_id", filters.RoutingGroupID)
		}
		if filters.ExternalID != "" {
			filterQuery.Add("external_id", filters.ExternalID)
		}
	}


	r, err := api.client.prepareRequest(ctx,
		localVarPath,
		localVarHTTPMethod,
		nil,
		nil,
		filterQuery,
		formParams,
		"",
		nil,
		)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := api.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	dpList := &[]*DialpeerOutput{}

	envel := &ResponseEnvelope{Data: dpList}

	err = readBody(localVarHttpResponse, envel)
	if err != nil {
		return nil, err
	}

	list = *dpList

	return list, err
}

//GetDialpeerByID gets a dialpeer by its ID
func (api *DialpeersService) GetDialpeerByID(ctx context.Context, id string) (dp *DialpeerOutput, err error) {
	if len(id) < 1 {
		return nil, reportError("dialpeer id must be more than 1 symbols")
	}

	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := api.client.cfg.Host + api.client.cfg.BasePath + "/dialpeers/" + id

	r, err := api.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := api.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	dp = &DialpeerOutput{}
	envel := &ResponseEnvelope{Data: dp}

	err = readBody(localVarHttpResponse, envel)
	if err != nil {
		return nil, err
	}

	return dp, err
}

//CreateDialpeer makes new entry in Dialpeers table of Yeti's database, passing given dialpeer struct as a body
func (api *DialpeersService) CreateDialpeer(ctx context.Context, d *DialpeerInput) (dp *DialpeerOutput, err error) {
	//TODO probably we need some security checks here

	localVarHttpMethod := "POST"

	// create path and map variables
	localVarPath := api.client.cfg.Host + api.client.cfg.BasePath + "/dialpeers"

	localVarBody, err := setBody(d, "application/json")
	if err != nil {
		return nil, err
	}

	headerParams := make(map[string]string)
	queryParams, formParams  := url.Values{}, url.Values{}

	r, err := api.client.prepareRequest(ctx,
		localVarPath,
		localVarHttpMethod,
		localVarBody,
		headerParams,
		queryParams,
		formParams,
		"",
		nil)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := api.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode == 201 {
		dp = &DialpeerOutput{}
		envel := &ResponseEnvelope{Data: dp}

		err = readBody(localVarHttpResponse, envel)
		if err != nil {
			return nil, err
		}
	} else {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	return dp, err
}

//DeleteDialpeer deletes a dialpeer entry by its ID
func (api *DialpeersService) DeleteDialpeer(ctx context.Context, id string) (err error) {
	if len(id) < 1 {
		return reportError("dialpeer id must be more than 1 symbols")
	}

	localVarHttpMethod := "DELETE"

	// create path and map variables
	localVarPath := api.client.cfg.Host + api.client.cfg.BasePath + "/dialpeers/" + id

	r, err := api.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return err
	}

	localVarHttpResponse, err := api.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return reportError("status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	return err
}
