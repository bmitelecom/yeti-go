package yetiapi

import (
	"context"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func TestAPIClient_Authenticate(t *testing.T) {
	mock := RunMockServer(t, "")

	ctx := context.Background()

	cfg := NewConfiguration()
	cfg.BasicAuth = &BasicAuth{UserName: "admin", Password: "admin"}
	cfg.Host = mock.URL

	clt, err := NewAPIClient(cfg)
	assert.NoError(t, err)

	clt.cfg.HTTPClient = mock.Client()

	err = clt.Authenticate(ctx)
	assert.NoError(t, err)
}

func TestAPIClient_parseJWT(t *testing.T) {

	token := "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODE2OTc5OTYsInN1YiI6MTd9.lBwLLvstlxAAJxdM0V0xPsGnjaTy38qpLEWlZ9mixfA"

	_, err := parseJWT(token)
	if err != nil {
		assert.EqualError(t, err, "token is invalid", "We check invalid token")
	}
}

func TestAPIClient_readBody(t *testing.T) {
	rc := ioutil.NopCloser(strings.NewReader(`{"data": {"jwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODE1MDQ2NTgsInN1YiI6MTd9.7Sz-GKZyP8Lea6ERRNqyQ59lnzxpDnQNBB0R3lekTJg"}}`))

	resp := &http.Response{
		Body: rc,
	}

	type testStruct struct {
		Foo string `json:"foo"`
	}

	ar := &AuthResponse{}

	err := readBody(resp, ar)
	if err != nil {
		assert.Error(t, err)
	}

	assert.Equal(t, ar.Token, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1ODE1MDQ2NTgsInN1YiI6MTd9.7Sz-GKZyP8Lea6ERRNqyQ59lnzxpDnQNBB0R3lekTJg", "Foo must be bar")
}
