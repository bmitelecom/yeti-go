package yetiapi

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func RunMockServer(t *testing.T, body string) (srv *httptest.Server) {

	mux := http.NewServeMux()

	mux.HandleFunc("/api/rest/admin/dialpeers/", func(w http.ResponseWriter, r *http.Request){
		t.Log("Request hit /api/rest/admin/dialpeers")

		w.Header().Add("Server", "Cowboy")
		w.Header().Add("Content-Language", "en")
		w.Header().Add("Etag", "6a5ea7b838e7b91c5a38bc2b1050a4fa")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Vary", "accept-language, accept")
		w.Header().Add("Date", "Mon, 12 Aug 2019 05:18:39 GMT")

		if r.Method == "POST" {
			w.WriteHeader(201)

			io.WriteString(w, body)
		} else {
			w.WriteHeader(200)

			io.WriteString(w, body)
		}
	})

	mux.HandleFunc("/api/rest/admin/auth", func(w http.ResponseWriter, r *http.Request){
		t.Log("Request hit /api/rest/admin/auth")

		w.Header().Add("Server", "Cowboy")
		w.Header().Add("Content-Language", "en")
		w.Header().Add("Etag", "6a5ea7b838e7b91c5a38bc2b1050a4fa")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Vary", "accept-language, accept")
		w.Header().Add("Date", "Mon, 12 Aug 2019 05:18:39 GMT")

		mySigningKey := []byte("AllYourBase")

		type MyCustomClaims struct {
			SUB string `json:"sub"`
			jwt.StandardClaims
		}

		// Create the Claims
		claims := MyCustomClaims{
			"123",
			jwt.StandardClaims{
				ExpiresAt: 15000,
				Issuer:    "test",
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		ss, err := token.SignedString(mySigningKey)
		if err != nil {
			w.WriteHeader(403)
		}

		body := fmt.Sprintf(`{"jwt": "%s"}`, ss)

		w.WriteHeader(201)

		io.WriteString(w, body)
	})

	srv = httptest.NewServer(mux)
	//defer srv.Close()

	t.Log("Mock server listens on: " + srv.URL)

	return srv
}
